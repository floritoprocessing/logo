class CommandListPreset {

  public static final int STAR1 = 0;
  public static final int FIRST_BOX = 1;
  public static final int STAR2 = 2;

  private CommandList[] commandListArray;

  CommandListPreset() {
    commandListArray = new CommandList[3];

    commandList = new CommandList();
    commandList.add(new Command(Command.LOOP_START,9));
    commandList.add(new Command(Command.SET_N,10));
    commandList.add(new Command(Command.LOOP_START,10));
    commandList.add(new Command(Command.FORWARD,Command.N));
    commandList.add(new Command(Command.FORWARD,Command.N));
    commandList.add(new Command(Command.RIGHT,Command.N));
    commandList.add(new Command(Command.RIGHT,10));
    commandList.add(new Command(Command.DECREASE_N,4));
    commandList.add(new Command(Command.LOOP_END));
    commandList.add(new Command(Command.RIGHT,180));
    commandList.add(new Command(Command.LOOP_END));
    commandListArray[0] = commandList;


    commandList = new CommandList();
    commandList.add(new Command(Command.SET_N,20));
    commandList.add(new Command(Command.LOOP_START,3));
    commandList.add(new Command(Command.LOOP_START,8));
    commandList.add(new Command(Command.LOOP_START,4));
    commandList.add(new Command(Command.FORWARD,Command.N));
    commandList.add(new Command(Command.RIGHT,90));
    commandList.add(new Command(Command.LOOP_END));
    commandList.add(new Command(Command.RIGHT,45));
    commandList.add(new Command(Command.LOOP_END));
    commandList.add(new Command(Command.INCREASE_N,50));
    commandList.add(new Command(Command.LOOP_END));
    commandListArray[1] = commandList;


    commandList = new CommandList();
    commandList.add(new Command(Command.LOOP_START,8));
    commandList.add(new Command(Command.SET_N,10));
    commandList.add(new Command(Command.LOOP_START,9));
    commandList.add(new Command(Command.FORWARD,Command.N));
    commandList.add(new Command(Command.RIGHT,15));
    commandList.add(new Command(Command.DECREASE_N,4));
    commandList.add(new Command(Command.LOOP_END));
    commandList.add(new Command(Command.LOOP_END));
    commandListArray[2] = commandList;

  }

  CommandList getCommandList(int i) {
    return commandListArray[i];
  }

}
