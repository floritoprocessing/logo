import java.util.Vector;

Turtle turtle;
CommandList commandList;
CommandListInterpreter commandListInterpreter;
CommandListPreset commandListPreset;

void setup() {
  size(640,480,P3D);
  
  turtle = new Turtle();
  
  turtle.penDown();
  turtle.moveTo(320,240);
  
  commandListPreset = new CommandListPreset();
  
  commandList = new CommandList();
  
  
  commandList.addPreset(commandListPreset,CommandListPreset.STAR2);
  //commandList.addPreset(commandListPreset,CommandListPreset.STAR1); //STAR1
  //commandList.addPreset(commandListPreset,CommandListPreset.FIRST_BOX); //STAR1
    
  commandListInterpreter = new CommandListInterpreter(commandList);
  commandListInterpreter.setSpeed(1);
  commandListInterpreter.precompileForLoops();
  
}

void draw() {
  background(0,0,0);
  
  commandListInterpreter.executeOn(turtle);
  turtle.show();
//  commandListInterpreter.step();
}
