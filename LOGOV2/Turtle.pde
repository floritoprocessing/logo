class Turtle {
  
  private double x=0;
  private double y=0;
  private double orientation = 0;
  private color COLOR = color(128,255,128);
  private color DRAW_COLOR = color(255,255,255);
  private boolean DRAWING = true;
  private Drawing drawing;
  
  Turtle() {
    drawing = new Drawing();
  }
  
  void penUp() {
    DRAWING = false;
  }
  
  void penDown() {
    DRAWING = true;
  }
  
  
  
  
  void turnRight(double o) {
    orientation += (2*Math.PI*o/360.0);
  }
  
  void moveForward(float v) {
    double xOffset = 0;
    double yOffset = -v;
    double SIN=Math.sin(orientation);
    double COS=Math.cos(orientation); 
    double xNew=x + xOffset*COS-yOffset*SIN; 
    double yNew=y + yOffset*COS+xOffset*SIN;
    x=xNew;
    y=yNew;
    drawing.addDrawPoint(new DrawPoint(x,y,DRAWING));
  }
  
  void moveTo(float _x, float _y) {
    x = _x;
    y = _y;
    drawing.addDrawPoint(new DrawPoint(x,y,DRAWING));
  }
  
  
  
  
  void show() {
    stroke(DRAW_COLOR);
    drawing.draw();
    
    pushMatrix();
      translate((float)x,(float)y);
      rotateZ((float)orientation);
      noFill();
      stroke(COLOR);
      line(0,-10,5,5);
      line(5,5,-5,5);
      line(-5,5,0,-10);
    popMatrix();
 
  }
}
