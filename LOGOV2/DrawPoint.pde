class DrawPoint {
  
  private double x = 0;
  private double y = 0;
  private boolean drawing = false;
  
  DrawPoint(double _x, double _y, boolean _drawing) {
    x = _x;
    y = _y;
    drawing = _drawing;
  }
  
  double getX() {
    return x;
  }
  
  double getY() {
    return y;
  }
  
  boolean isDown() {
    return drawing;
  }
  
}
