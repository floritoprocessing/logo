class Command {
  
  private int COMMAND = 0;
  private float VALUE = 0;
  
  public static final int FORWARD=0;
  public static final int RIGHT=1;
  public static final int SET_N=2;
  public static final int INCREASE_N=3;
  public static final int DECREASE_N=4;
  public static final int LOOP_START=5;
  public static final int LOOP_END=6;
  
  public static final int N = -999999;
  
  Command() {
  }
  
  Command(int c) {
    COMMAND = c;
    VALUE = 0;
  }

  Command(int c, float v) {
    COMMAND = c;
    VALUE = v;
    
    if (v==N) VALUE = N;
  }
  
  float getValue() {
    return VALUE;
  }
  
  boolean is(int c) {
    return (COMMAND==c);
  }
  
  String toString() {
    String out = "";
    switch (COMMAND) {
      case FORWARD: out = "FORWARD "+VALUE; break;
      case RIGHT: out = "RIGHT "+VALUE; break;
      case SET_N: out = "SET N to "+VALUE; break;
      default: out = ""; break;
    }
    return out;
  }
}
