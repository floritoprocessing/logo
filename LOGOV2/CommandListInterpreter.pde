class CommandListInterpreter {
  
  private CommandList commandList;
  private float SPEED = 1;
  
  private float progress = 0;
  private int commandIndex = 0;
  private int lastCommandIndex = -1;
  private boolean RUNNING = true;
  
  private int loopIndex=0;
  private int nrOfLoops=0;
  private LoopInfo[] loopInfo;
  
  private int N = 0;
  
  CommandListInterpreter(CommandList cl) {
    
    commandList = cl;
    
    // check syntax:
    int nrOfLoopStart=0;
    int nrOfLoopEnd=0;
    for (int i=0;i<cl.getSize();i++) {
      if (cl.getCommand(i).is(Command.LOOP_START)) nrOfLoopStart++;
      if (cl.getCommand(i).is(Command.LOOP_END)) nrOfLoopEnd++;
    }
    if (nrOfLoopStart!=nrOfLoopEnd) {
      println("MISMATCHING NUMBER OF LOOP START/END!");
      commandList = new CommandList();
    } else {
      nrOfLoops = nrOfLoopStart;
    }
  }
  
  
  
  void precompileForLoops() {
    loopInfo = new LoopInfo[nrOfLoops];
    int[] startIndex = new int[nrOfLoops];
    int[] startValue = new int[nrOfLoops];
    int[] endIndex = new int[nrOfLoops];
    
    int loopNr=0;
    for (int i=0;i<commandList.getSize();i++) {
      if (commandList.getCommand(i).is(Command.LOOP_START)) {
        startIndex[loopNr] = i;
        startValue[loopNr] = (int)((commandList.getCommand(i)).getValue());
        loopNr++;
      }
    }
    loopNr = nrOfLoops-1;
    for (int i=0;i<commandList.getSize();i++) {
      if (commandList.getCommand(i).is(Command.LOOP_END)) {
        endIndex[loopNr] = i;
        loopNr--;
      }
    }
    
    for (int i=0;i<nrOfLoops;i++) {
      loopInfo[i] = new LoopInfo(startIndex[i],endIndex[i],startValue[i]);
    }
  }
  
  
  
  void setSpeed(float s) {
    SPEED = s;
    if (SPEED>1.0) SPEED=1.0;
  }
  
  void executeOn(Turtle t) {
    if (commandIndex>=commandList.getSize()) RUNNING = false;
    
    if (RUNNING) {
      
      Command currentCommand = commandList.getCommand(commandIndex);
      //println(currentCommand);
      
      
      if (currentCommand.is(Command.FORWARD)) {
        float distance = currentCommand.getValue();
        if (distance==Command.N) distance = N;
        t.moveForward(distance*SPEED);
        step();
      }
      
      
      if (currentCommand.is(Command.RIGHT)) {
        float degree = currentCommand.getValue();
        if (degree==Command.N) degree = N;
        t.turnRight(degree*SPEED);
        step();
      }
      
      
      if (currentCommand.is(Command.SET_N)) {
        N = (int)currentCommand.getValue();
        fullStep();
      }
      
      if (currentCommand.is(Command.INCREASE_N)) {
        N += (int)currentCommand.getValue();
        fullStep();
      }
      
      if (currentCommand.is(Command.DECREASE_N)) {
        N -= (int)currentCommand.getValue();
        fullStep();
      }
      
      if (currentCommand.is(Command.LOOP_START)) {
        //println("loop start at: "+commandIndex);
        fullStep();
      }
      
      if (currentCommand.is(Command.LOOP_END)) {
        int in=-1;
        int out=-1;
        for (int i=0;i<nrOfLoops;i++) {
          if (loopInfo[i].getOutPoint()==commandIndex) {
            if (loopInfo[i].getNrOfLoops()>1) {
              loopInfo[i].decreaseNumberOfLoops();
              stepTo(loopInfo[i].getInPoint());
            } else {
              if (i>0) loopInfo[i].resetNumberOfLoops();
              fullStep();
            }
          }
        }
        //fullStep();
      }
      
    }
  }
  
  void step() {
    progress += SPEED;
    if (progress>=1) {
      progress = 0;
      commandIndex ++;
    }
  }
  
  void stepTo(int i) {
    commandIndex=i;
    progress = 0;
  }
  
  void fullStep() {
    commandIndex++;
    progress = 0;
  }
  
  
  
}
