class LoopInfo {
  
  private int inPoint;
  private int outPoint;
  private int nrOfLoops;
  private int initNrOfLoops;
  
  LoopInfo(int i,int o, int no) {
    inPoint = i;
    outPoint = o;
    nrOfLoops = no;
    initNrOfLoops = no;
    
    println("New loop info:");
    println(" in:  "+inPoint);
    println(" out: "+outPoint);
    println(" repeats: "+nrOfLoops);
  }
  
  void resetNumberOfLoops() {
    nrOfLoops = initNrOfLoops;
  }
  
  void decreaseNumberOfLoops() {
    nrOfLoops--;
  }
  
  int getNrOfLoops() {
    return nrOfLoops;
  }
  
  int getInPoint() {
    return inPoint;
  }
  
  int getOutPoint() {
    return outPoint;
  }
  
}
