class Drawing {
  
  Vector drawPoints;
  
  Drawing() {
    drawPoints = new Vector();
  }
  
  void addDrawPoint(DrawPoint dp) {
    drawPoints.add(dp);
  }
  
  void draw() {
    boolean drawing = false;
    for (int i=0;i<drawPoints.size()-1;i++) {
      DrawPoint dp0 = (DrawPoint)(drawPoints.elementAt(i));
      DrawPoint dp1 = (DrawPoint)(drawPoints.elementAt(i+1));
      if (dp0.isDown()) {
        line((float)dp0.getX(),(float)dp0.getY(),(float)dp1.getX(),(float)dp1.getY());
      }
    }
  }
  
}
