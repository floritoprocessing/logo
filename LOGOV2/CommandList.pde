class CommandList {
  
  private Vector commands;
  
  CommandList() {
    commands = new Vector();
  }
  
  void add(Command c) {
    commands.add(c);
  }
  
  void addPreset(CommandListPreset clp,int ix) {
   CommandList cList = clp.getCommandList(ix);
   for (int i=0;i<cList.getSize();i++) {
     commands.add((Command)cList.getCommand(i));
   }
  }
  
  int getSize() {
    return commands.size();
  }
  
  Command getCommand(int i) {
    return (Command)(commands.elementAt(i));
  }
    
}
